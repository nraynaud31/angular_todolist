import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // 1 Objet par routes
  // {
  //   // Path et components sont obligatoires
  //   path: 'todo',
  //   component: TodoComponent,

  //   // canActivate => permet de lancer un Component, fonction d'un service ou autre.
  //   // canActivate: [AuthGuard]

  //   // data => données à transmettre au composant
  //   // data: {
  //   //   breadcrumb: 'Paramètres'
  //   // }
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
