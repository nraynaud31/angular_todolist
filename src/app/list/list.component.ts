import { Component, OnInit } from '@angular/core';
import Task from '../model/task.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor() { }

  public list: Task[] = [];

  ngOnInit(): void {
  }

  addTodo() {
    this.list.push(new Task());
  }

  removeTask(index: number) {
    if (index > -1) {
      this.list.splice(index, 1);
    }
  }
}
